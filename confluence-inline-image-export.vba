Sub aaaFixDocumentIncBorders()

'If the macro finishes but misses some icons, you will need to add the image file pages to the macro as new subs (copy and paste an existing sub and make sure you update the `Call` commands. After doing this, run the macro again from the `aaFixDocumentIgnoreBorders` sub, otherwise all inline images will get a border.

Call UpdateBorders

End Sub

Sub UpdateBorders()

'Adds a border to all images in the document

Dim i As Long, j As Long
With ActiveDocument.Range
    For i = 1 To .InlineShapes.Count
        With .InlineShapes(i)
            For j = 1 To .Borders.Count
                .Borders(j).LineStyle = wdLineStyleSingle
                .Borders(j).Color = wdColorAutomatic
            Next j
        End With
    Next i
End With

Call aaFixDocumentIgnoreBorders

End Sub

Sub aaFixDocumentIgnoreBorders()

Call <NEXT_SUB_NAME>

End Sub

Sub <SUB_NAME>()

'Name the sub after the icon being replaced. Add a new sub for each icon to replace (unless you can think of a way to replace all icons in a single sub...)

Dim n, c As Integer
n = Application.Documents.Count
c = 1

Dim r As Range

Windows(c).Activate

'The imageFullPath value is the file path to the image you need to insert
' The FindText value is the emoji name. It will begin and end with `:`. The macro searches for the emoji name and replaces it with the file specified by the imageFullPath value.

Do
Dim imageFullPath As String
Dim FindText As String
imageFullPath = "<PATH\TO\ICON\IMAGE\FILE.png"
FindText = ":<EMOJI_NAME_IN_CONFLUENCE>:"
    With Selection
    .HomeKey Unit:=wdStory

    With .Find
        .ClearFormatting
        .Text = FindText
        ' Loop until Word can no longer
        ' find the search string, inserting the specified image at each location
        Do While .Execute
            Selection.Delete
            Selection.InlineShapes.AddPicture FileName:=imageFullPath, LinkToFile:=False, SaveWithDocument:=True
        Loop

    End With
End With


    c = c + 1

    On Error Resume Next
    Windows(c).Activate

Loop Until c > n

Call SelectAllRefresh

End Sub

Sub SelectAllRefresh()

'This sub refreshes the document to update the table of content (if present).

    Selection.WholeStory
    Selection.Fields.Update

End Sub

