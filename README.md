# Miscellaneous Code

## Confluence-inline-image-export.vba

Bodge jobs to workaround bodge jobs... A rough macro to add borders to images and insert images where Confluence required emoji.

Prerequisites:

*  The filepath for all images uploaded as emoji
*  the emoji code (for example `:emoji:`)
*  Patience
*  A Word template for Confluence exports

So this macro adds borders to any images exported from Confluence, and then looks for any emoji code you have specified and replaces it with an image held on file at the path specified. I've not yet worked out a better way of doing it, so it is a verrry long macro, with a sub for each emoji. 

Disclaimer: I can't remember how I cobbled together the macro, so I won't try to claim that it's my own work or take credit for it. Test it before using it for real, because I don't want to take any blame for it either! Refactors welcome...!

See below for the process I follow to export from Confluence to Word (just, don't ask why I need to do all this...)

1. Export the Confluence page to Word.
2. Open the exported document (“Export”).
3. Open a new document from the Confluence Export Word template (“New”).
4. In the Export document:
   1. Select all content from the beginning of the Table of Contents line to the end of the Zendesk link line and delete it.
      (This is just any Confluence-specific content that does not go in the final PDF)
   2. Select all content and change the font to Calibri.
   3. Copy all the content.
5. In the New document:
   1. On the third page, paste the copied content over the `{Paste content here}` line.
   2. On the cover page, edit the `{TITLE}`.
   3. For all heading styles that appear in the document:
      1. Select all instances
      2. Reapply the heading style
   4. Close the Export document.
6. Run the aaaFixDocumentIncBorders macro.
   This performs the following actions:
   * Add borders to images
   * Replace emoji shortcuts with images
   * refreshes page to populate the Table of Contents
7. **Ctrl + F** ` :`, `:ic`, and `:but` to check that no `:icon-` or `:button-` inline emoji have been missed by the macro.
   If any emoji have not been converted by the macro, you need to add a new Sub to the macro for each one that contains the emoji shortcut `:icon-example:` and the filename of the corresponding image.
8. If you had to add any new emoji conversion Subs, run the aaFixDocumentIgnoringBorders macro.
   This skips the image border step, so the icon and button images do not get borders.


